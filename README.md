# Cosign Sign/Verify Images

> **Note**
>
> This component is work-in-progress, where inputs, template names, and interfaces might change through 0.x.y releases.
> Please wait for 1.x.y for production usage.

This CI/CD component provides job templates to sign and verify container images with sigstore cosign. You can specify the Container Image to be signed or verified as input

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml` file by using the `include:`
keyword, where `<VERSION>` is the latest released tag or `main`.

### Individual job templates

Individual jobs are available as separate component templates: `sign`.

```yaml
include:
  # sign
  - component: gitlab.com/cpanato/cosign-sign-verify/sign@<VERSION>
    inputs:
      stage: sign
      image_to_sign: <image>

```

#### Inputs

##### Sign

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `format` | The format stage name |
| `image_to_sign` | `` | The image to be signed, prefereable in with the digest instead of a tag  |
